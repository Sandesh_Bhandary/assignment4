package com.example.carcrud.controller;

import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.when;

import com.example.carcrud.entity.Car;
import com.example.carcrud.service.CarService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {CarController.class})
@ExtendWith(SpringExtension.class)
class CarControllerTest {
    @Autowired
    private CarController carController;

    @MockBean
    private CarService carService;

    
    @Test
    void testAddCar() throws Exception {
        Car car = new Car();
        car.setId(1);
        car.setManufacturer("Manufacturer");
        car.setModel("Model");
        car.setStatus("Status");
        car.setYom("Yom");
        String content = (new ObjectMapper()).writeValueAsString(car);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cars")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(carController).build().perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(405));
    }

    
    @Test
    void testGetCar() throws Exception {
        Car car = new Car();
        car.setId(1);
        car.setManufacturer("Manufacturer");
        car.setModel("Model");
        car.setStatus("Status");
        car.setYom("Yom");
        when(carService.getCar(anyInt())).thenReturn(car);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cars/{carid}", 1);
        MockMvcBuilders.standaloneSetup(carController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"manufacturer\":\"Manufacturer\",\"model\":\"Model\",\"yom\":\"Yom\",\"status\":\"Status\",\"id\":1}"));
    }
}

