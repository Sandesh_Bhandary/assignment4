package com.example.carcrud.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class CarTest {
   
    @Test
    void testConstructor() {
        Car actualCar = new Car();
        actualCar.setId(1);
        actualCar.setManufacturer("Manufacturer");
        actualCar.setModel("Model");
        actualCar.setStatus("Status");
        actualCar.setYom("Yom");
        assertEquals(1, actualCar.getId());
        assertEquals("Manufacturer", actualCar.getManufacturer());
        assertEquals("Model", actualCar.getModel());
        assertEquals("Status", actualCar.getStatus());
        assertEquals("Yom", actualCar.getYom());
    }
}

