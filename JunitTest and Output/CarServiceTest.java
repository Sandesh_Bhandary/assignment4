package com.example.carcrud.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.carcrud.entity.Car;
import com.example.carcrud.repository.CarRepository;

import java.util.Optional;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {CarService.class})
@ExtendWith(SpringExtension.class)
class CarServiceTest {
    @MockBean
    private CarRepository carRepository;

    @Autowired
    private CarService carService;

   
    @Test
    void testSave() {
        Car car = new Car();
        car.setId(1);
        car.setManufacturer("Manufacturer");
        car.setModel("Model");
        car.setStatus("Status");
        car.setYom("Yom");
        when(carRepository.save((Car) any())).thenReturn(car);

        Car car1 = new Car();
        car1.setId(1);
        car1.setManufacturer("Manufacturer");
        car1.setModel("Model");
        car1.setStatus("Status");
        car1.setYom("Yom");
        assertSame(car, carService.save(car1));
        verify(carRepository).save((Car) any());
    }

    
    @Test
    void testGetCar() {
        Car car = new Car();
        car.setId(1);
        car.setManufacturer("Manufacturer");
        car.setModel("Model");
        car.setStatus("Status");
        car.setYom("Yom");
        Optional<Car> ofResult = Optional.of(car);
        when(carRepository.findById((Integer) any())).thenReturn(ofResult);
        assertSame(car, carService.getCar(1));
        verify(carRepository).findById((Integer) any());
    }

    @Test
    @Disabled("TODO: Complete this test")
    void testGetCar2() {
        
        when(carRepository.findById((Integer) any())).thenReturn(Optional.empty());
        carService.getCar(1);
    }

   
    @Test
    void testUpdate() {
        Car car = new Car();
        car.setId(1);
        car.setManufacturer("Manufacturer");
        car.setModel("Model");
        car.setStatus("Status");
        car.setYom("Yom");
        Optional<Car> ofResult = Optional.of(car);

        Car car1 = new Car();
        car1.setId(1);
        car1.setManufacturer("Manufacturer");
        car1.setModel("Model");
        car1.setStatus("Status");
        car1.setYom("Yom");
        when(carRepository.save((Car) any())).thenReturn(car1);
        when(carRepository.findById((Integer) any())).thenReturn(ofResult);

        Car car2 = new Car();
        car2.setId(1);
        car2.setManufacturer("Manufacturer");
        car2.setModel("Model");
        car2.setStatus("Status");
        car2.setYom("Yom");
        assertSame(car1, carService.update(car2));
        verify(carRepository).save((Car) any());
        verify(carRepository).findById((Integer) any());
    }

    @Test
    @Disabled("TODO: Complete this test")
    void testUpdate2() {
       

        Car car = new Car();
        car.setId(1);
        car.setManufacturer("Manufacturer");
        car.setModel("Model");
        car.setStatus("Status");
        car.setYom("Yom");
        when(carRepository.save((Car) any())).thenReturn(car);
        when(carRepository.findById((Integer) any())).thenReturn(Optional.empty());

        Car car1 = new Car();
        car1.setId(1);
        car1.setManufacturer("Manufacturer");
        car1.setModel("Model");
        car1.setStatus("Status");
        car1.setYom("Yom");
        carService.update(car1);
    }

    @Test
    void testDelete() {
        doNothing().when(carRepository).deleteById((Integer) any());
        assertEquals("deleted", carService.delete(1).getStatus());
        verify(carRepository).deleteById((Integer) any());
    }

    
    @Test
    void testConstructor() {
        
        (new CarService()).setCarRepository(mock(CarRepository.class));
    }
}

